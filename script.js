//-----------------------------------ЗАДАЧА---ПЕРША-------------------------------------

// const level = Math.floor(Math.random() * 101);
// if (level >= 95) {
//   // console.log(level);
//   alert(level + '%' + '  Чудово,заряд повний.');
// } else if (level >= 60) {
//   // console.log(level);
//   alert(level + '%' + '  Згадай де твоя зарядка,але можна не спішити.');
// } else if (level >= 30) {
//   // console.log(level);
//   alert(level + '%' + '  Можна потерпіти,але не довго!.');
// } else if (level >= 5) {
//   // console.log(level);
//   alert(level + '%' + '  Вже здихаю.');
// } else if (level >= 0) {
//   // console.log(level);
//   alert(level + '%' + '  Розрядився!.');
// }

//--------------------------------------------------------------------------------------
//-----------------------------------ЗАДАЧА---ДРУГА-------------------------------------

// let a = 1;
// a = a + 1;

// let b = 40;
// b += 15;

// let c = 2;
// c++;

// let d = 20;
// d = a + d;

// console.log(a);
// console.log(b);
// console.log(c);
// console.log(d);

//--------------------------------------------------------------------------------------
//-----------------------------------ЗАДАЧА---ТРЕТЯ-------------------------------------

const countDownDate = new Date('January 01, 2023 00:00:00').getTime();

const x = setInterval(function () {
  const now = new Date().getTime();
  const distance = countDownDate - now;

  const days = Math.floor(distance / (1000 * 60 * 60 * 24));
  const hours = Math.floor(
    (distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)
  );
  const minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  const seconds = Math.floor((distance % (1000 * 60)) / 1000);

  document.getElementById('demo').innerHTML =
    'До нового року залишилось : ' +
    days +
    'd ' +
    hours +
    'h ' +
    minutes +
    'm ' +
    seconds +
    's ';

  if (distance < 0) {
    clearInterval(x);
    document.getElementById('demo').innerHTML = 'З НОВИМ РОКОМ!!!';
  }
}, 1000);

//--------------------------------------------------------------------------------------
